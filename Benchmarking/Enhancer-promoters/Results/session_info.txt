R version 4.0.3 (2020-10-10)
Platform: x86_64-conda-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /mnt/irisgpfs/users/cbarlier/libraries/miniconda3/envs/r4-base-clone1/lib/libopenblasp-r0.3.12.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] dplyr_1.0.8       ggplot2_3.3.5     data.table_1.14.2

loaded via a namespace (and not attached):
 [1] fansi_0.5.0      withr_2.4.2      assertthat_0.2.1 crayon_1.4.1    
 [5] utf8_1.2.1       grid_4.0.3       R6_2.5.0         DBI_1.1.1       
 [9] lifecycle_1.0.1  gtable_0.3.0     magrittr_2.0.1   scales_1.1.1    
[13] pillar_1.6.1     rlang_1.0.2      cli_3.0.0        generics_0.1.0  
[17] vctrs_0.3.8      ellipsis_0.3.2   glue_1.4.2       purrr_0.3.4     
[21] munsell_0.5.0    compiler_4.0.3   pkgconfig_2.0.3  colorspace_2.0-2
[25] tidyselect_1.1.1 tibble_3.1.2    
