#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
#Arg1: Cicero rds object
#Arg2: analysis path
#Arg3: genehancer file
#Arg4: chip file
#Arg5: scRNA-seq genes
#Arg6: final output file name
library(Matrix)
library(stringr)
library(data.table)
#Doc: https://cole-trapnell-lab.github.io/cicero-release/docs/

#Load rds object
ds <- readRDS(args[1])

#Keep co-accessibility > 0 as in their documentation
ds <- ds[which(ds$coaccess > 0),]

#Write table with source peaks as enhancers bed coordinates
ds$chr <- sapply(ds$Peak1,function(x){strsplit(x,split="_")[[1]][1]})
ds$start <- sapply(ds$Peak1,function(x){strsplit(x,split="_")[[1]][2]})
ds$end <- sapply(ds$Peak1,function(x){strsplit(x,split="_")[[1]][3]})
ds <- ds[,c("chr","start","end","Peak2","coaccess")]
write.table(ds,paste0(args[2],"tmpCicero.bed"),sep="\t",row.names=F,col.names=F,quote=F)
rm(ds)

#Annotate enhancers regions using GeneHancer
system(paste0("sort -k1,1 -k2,2n ",args[2],"tmpCicero.bed > ",args[2],"sorted.tmpCicero.bed"))
system(paste0("bedtools intersect -a ",paste0(args[2],"sorted.tmpCicero.bed")," -b ",args[3]," -F 1.0 -wo -sorted > ",paste0(args[2],"EnhCicero.txt")))
system(paste0("rm ",args[2],"tmpCicero.bed"))
system(paste0("rm ",args[2],"sorted.tmpCicero.bed"))

#Load the file & select enhancer regions + Peak 2 & coaccessibility score
ds <- fread(paste0(args[2],"EnhCicero.txt"))
colnames(ds) <- c("Chr","Start","Stop","Peak2","coaccess","EnhChr","EnhStart","EnhStop","Type","X")
ds$Chr <- NULL
ds$Start <- NULL
ds$Stop <- NULL
ds$Type <- NULL
ds$Enhancer <- paste(ds$EnhChr,paste(ds$EnhStart,ds$EnhStop,sep="-"),sep=":")
ds <- ds[,c("Peak2","Enhancer","coaccess")]
#Write table with target peaks as promoter bed coordinates
ds$chr <- sapply(ds$Peak2,function(x){strsplit(x,split="_")[[1]][1]})
ds$start <- sapply(ds$Peak2,function(x){strsplit(x,split="_")[[1]][2]})
ds$end <- sapply(ds$Peak2,function(x){strsplit(x,split="_")[[1]][3]})
ds <- ds[,c("chr","start","end","Enhancer","coaccess")]
write.table(ds,paste0(args[2],"tmpCicero.bed"),sep="\t",row.names=F,col.names=F,quote=F)
rm(ds)

#Annotate promoters to retrieve connected gene
system(paste0("sort -k1,1 -k2,2n ",args[2],"tmpCicero.bed > ",args[2],"sorted.tmpCicero.bed"))
system(paste0("bedtools intersect -a ",paste0(args[2],"sorted.tmpCicero.bed")," -b ",args[4]," -F 1.0 -wo -sorted > ",paste0(args[2],"PromCicero.txt")))
system(paste0("rm ",args[2],"tmpCicero.bed"))
system(paste0("rm ",args[2],"sorted.tmpCicero.bed"))

#Load the file & save network
ds <- fread(paste0(args[2],"PromCicero.txt"))
colnames(ds) <- c("chr","start","end","Enhancer","coaccess","ChrChip","StartChip","StopChip","Gene","X")
ds <- ds[,c("Enhancer","Gene","coaccess")]
#Select genes that were in scRNA-seq to make the comparison fair with RNetDys
g <- readRDS(args[5])
ds <- ds[which(ds$Gene %in% g),]
colnames(ds) <- c("Source","Target","Score")
write.table(ds,args[6],sep="\t",row.names=F,col.names=T,quote=F)

system(paste0("rm ",args[2],"EnhCicero.txt"))
system(paste0("rm ",args[2],"PromCicero.txt"))

