#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base

OUTPUTPATH="/scratch/users/cbarlier/GRN2021/Cicero/"

## GM12878

DATAPATHGM="/scratch/users/cbarlier/GRN2021/RNetDysNet/DATA/GM12878/scATACseq/"

#ATAC
ATAC_GM1=$DATAPATHGM"GM12878_scATAC_scOPEN_hg19.txt"
ATAC_GM2=$DATAPATHGM"hg19_GSE99172_scATACseq_peaksMtx.txt"

#SAVE NAME
GM_PN1=$OUTPUTPATH"GM_d1.rds"
GM_PN2=$OUTPUTPATH"GM_d2.rds"

Rscript runCicero.R $ATAC_GM1 $GM_PN1 "FALSE"
Rscript runCicero.R $ATAC_GM2 $GM_PN2 "FALSE"


## H1-ESC

DATAPATHH1="/scratch/users/cbarlier/GRN2021/RNetDysNet/DATA/H1ESC/scATACseq/"

#ATAC
ATAC_H1ESC1=$DATAPATHH1"H1ESC_scATAC_scOPEN_hg19.txt"
ATAC_H1ESC2=$DATAPATHH1"hg19_GSE99172_scATACseq_peaksMtx.txt"

#SAVE NAME
H1_PN1=$OUTPUTPATH"H1_d1.rds"
H1_PN2=$OUTPUTPATH"H1_d2.rds"

Rscript runCicero.R $ATAC_H1ESC1 $H1_PN1 "FALSE"
Rscript runCicero.R $ATAC_H1ESC2 $H1_PN2 "FALSE"

## BJ

DATAPATHBJ="/scratch/users/cbarlier/GRN2021/RNetDysNet/DATA/BJ/scATACseq/"

#ATAC
ATAC_BJ1=$DATAPATHBJ"BJ_scATAC_scOPEN_hg19.txt"
ATAC_BJ2=$DATAPATHBJ"hg19_GSE99172_BJ_scATACseq_peaksMtx.txt"

#SAVE NAME
BJ_PN1=$OUTPUTPATH"BJ_d1.rds"
BJ_PN2=$OUTPUTPATH"BJ_d2.rds"

Rscript runCicero.R $ATAC_BJ1 $BJ_PN1 "TRUE"
Rscript runCicero.R $ATAC_BJ2 $BJ_PN2 "FALSE"

conda deactivate