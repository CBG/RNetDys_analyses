#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

PATHDATA="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/SCENIC/TFGene/"
GSPATH="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/GS/"
OUTPUT="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PERF/"

## BJ
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE113415_BJ_FPKM.txt" $GSPATH"BJ_GSnetwork_TFGene.rds" $OUTPUT"BJ1_pref.txt" $PATHDATA"TFGene_50exp_GSE113415_BJ_FPKM.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE160910_single_count_BJ.txt" $GSPATH"BJ_GSnetwork_TFGene.rds" $OUTPUT"BJ2_pref.txt" $PATHDATA"TFGene_50exp_GSE160910_single_count_BJ.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt" $GSPATH"BJ_GSnetwork_TFGene.rds" $OUTPUT"BJ3_pref.txt" $PATHDATA"TFGene_50exp_GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE100344_BJ_FPKM.txt" $GSPATH"BJ_GSnetwork_TFGene.rds" $OUTPUT"BJ4_pref.txt" $PATHDATA"TFGene_50exp_GSE100344_BJ_FPKM.txt.csv" 2

## GM12878
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE81861_GM12878_count_batch2.txt" $GSPATH"GM12878_GSnetwork_TFGene.rds" $OUTPUT"GM1_pref.txt" $PATHDATA"TFGene_50exp_GSE81861_GM12878_count_batch2.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM4156602_GM12878_count_rep2.txt" $GSPATH"GM12878_GSnetwork_TFGene.rds" $OUTPUT"GM2_pref.txt" $PATHDATA"TFGene_50exp_GM12878_GSM4156602_GM12878_count_rep2.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM3596321_GM12878.txt" $GSPATH"GM12878_GSnetwork_TFGene.rds" $OUTPUT"GM3_pref.txt" $PATHDATA"TFGene_50exp_GSM3596321_GM12878.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM4156603_GM12878_count_rep3.txt" $GSPATH"GM12878_GSnetwork_TFGene.rds" $OUTPUT"GM4_pref.txt" $PATHDATA"TFGene_50exp_GM12878_GSM4156602_GM12878_count_rep3.txt.csv" 2

## H1-ESC
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE64016_H1ESC_normalized.txt" $GSPATH"H1ESC_GSnetwork_TFGene.rds" $OUTPUT"H1_pref.txt" $PATHDATA"TFGene_50exp_GSE64016_H1ESC_normalized.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE75748_H1ESC.txt" $GSPATH"H1ESC_GSnetwork_TFGene.rds" $OUTPUT"H2_pref.txt" $PATHDATA"TFGene_50exp_GSE75748_H1ESC.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE81861_H1ESC_count_batch1.txt" $GSPATH"H1ESC_GSnetwork_TFGene.rds" $OUTPUT"H3_pref.txt" $PATHDATA"TFGene_50exp_GSE81861_H1ESC_count_batch1.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM5534158_H1.txt" $GSPATH"H1ESC_GSnetwork_TFGene.rds" $OUTPUT"H4_pref.txt" $PATHDATA"TFGene_50exp_GSM5534158_H1.txt.csv" 2

## A549
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE81861_A549_scRNAseq.txt" $GSPATH"A549_GSnetwork_TFGene.rds" $OUTPUT"A1_pref.txt" $PATHDATA"TFGene_50exp_GSE81861_A549_scRNAseq.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM3271042_scRNAseq_A549.txt" $GSPATH"A549_GSnetwork_TFGene.rds" $OUTPUT"A2_pref.txt" $PATHDATA"TFGene_50exp_GSM3271042_scRNAseq_A549.txt.csv" 2

## Jurkat
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_Jurkat_GSE105451_scRNAseq.txt" $GSPATH"Jurkat_GSnetwork_TFGene.rds" $OUTPUT"Ju1_pref.txt" $PATHDATA"TFGene_50exp_Jurkat_GSE105451_scRNAseq.txt.csv" 1
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_Jurkat_scRNAseq.txt" $GSPATH"Jurkat_GSnetwork_TFGene.rds" $OUTPUT"Ju2_pref.txt" $PATHDATA"TFGene_50exp_Jurkat_scRNAseq.txt.csv" 1

## K-562
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE113415_K562_FPKM.txt" $GSPATH"K562_GSnetwork_TFGene.rds" $OUTPUT"K1_pref.txt" $PATHDATA"TFGene_50exp_GSE113415_K562_FPKM.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE81861_K562_count.txt" $GSPATH"K562_GSnetwork_TFGene.rds" $OUTPUT"K2_pref.txt" $PATHDATA"TFGene_50exp_GSE81861_K562_count.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSE90063_k562_umi_wt.txt" $GSPATH"K562_GSnetwork_TFGene.rds" $OUTPUT"K3_pref.txt" $PATHDATA"TFGene_50exp_GSE90063_k562_umi_wt.txt.csv" 2
Rscript run_benchPerf.R "TFGene_Network_inference_TOREPLACE_50exp_GSM1599500_K562_cells.txt" $GSPATH"K562_GSnetwork_TFGene.rds" $OUTPUT"K4_pref.txt" $PATHDATA"TFGene_50exp_GSM1599500_K562_cells.txt.csv" 2

conda deactivate