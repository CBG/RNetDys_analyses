#!/usr/bin/julia

#Installing and loading packages
#import Pkg
#Pkg.add("NetworkInference")
#Pkg.add("LightGraphs")
#Pkg.add("GraphPlot")
#Pkg.add("ReadWriteDlm2")

using NetworkInference
using LightGraphs
using GraphPlot
using DelimitedFiles

# Loading data
dataset_name = string(ARGS[1])

# Choose an algorithm
# PIDCNetworkInference(), PUCNetworkInference(), CLRNetworkInference() or MINetworkInference()
algorithm = PIDCNetworkInference()

# Get the genes and discretize the expression levels
@time genes = get_nodes(dataset_name);

# Infer the network
@time network = InferredNetwork(algorithm, genes);
write_network_file(ARGS[2], network)

