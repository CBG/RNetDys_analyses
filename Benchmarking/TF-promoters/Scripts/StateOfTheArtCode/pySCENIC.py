#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 17:38:52 2020

@author: celine.barlier
"""

import sys
import os
import glob
import pickle
import pandas as pd #version 0.25.3
import numpy as np #version 1.17.2
import scanpy as sc #version 1.4.6
import anndata as ad #version 0.7.1

from dask.diagnostics import ProgressBar

from arboreto.utils import load_tf_names
from arboreto.algo import grnboost2

from pyscenic.rnkdb import FeatherRankingDatabase as RankingDatabase
from pyscenic.utils import modules_from_adjacencies, load_motifs
from pyscenic.prune import prune2df, df2regulons
from pyscenic.aucell import aucell

import umap
#from MulticoreTSNE import MulticoreTSNE as TSNE

import seaborn as sns

#Script parameters
#1 = folder for the data
#2 = data file name .txt
#3 = perform normalization TRUE or FALSE

if __name__ == '__main__':

	
	DATA_FOLDER=sys.argv[1]
	RESOURCES_FOLDER="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/SCENIC/ressources/"
	DATABASE_FOLDER = "/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/SCENIC/databases/"
	#SCHEDULER="123.122.8.24:8786"
	DATABASES_GLOB = os.path.join(DATABASE_FOLDER, "hg19-*.mc9nr.feather")
	MOTIF_ANNOTATIONS_FNAME = os.path.join(RESOURCES_FOLDER, "motifs-v9-nr.hgnc-m0.001-o0.0.tbl")
	MM_TFS_FNAME = os.path.join(RESOURCES_FOLDER, "hs_hgnc_curated_tfs.txt")
	SC_EXP_FNAME = os.path.join(DATA_FOLDER, sys.argv[2]) 
	REGULONS_FNAME = os.path.join(DATA_FOLDER, "regulons.csv")
	MOTIFS_FNAME = os.path.join(DATA_FOLDER, "motifs.csv")
	AUCMATRIX_FNAME = os.path.join(DATA_FOLDER, "aucMatrix.csv")
	OUTFOLDER=sys.argv[4] 

    #Load data: cells in columns, genes in rows
	ex_matrix = pd.read_csv(SC_EXP_FNAME, sep=',', header=0, index_col=0).T
	ex_matrix.shape
    
    #create annData oject
	adata = ad.AnnData(ex_matrix)
    
    # initial cuts
	sc.pp.filter_cells(adata, min_genes=200)
	sc.pp.filter_genes(adata, min_cells=3)
    
    # Total-count normalize (library-size correct) to 10,000 reads/cell
	if sys.argv[3] == True:
		sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)

    # log transform the data
	sc.pp.log1p(adata)

	tf_names = load_tf_names(MM_TFS_FNAME)

	db_fnames = glob.glob(DATABASES_GLOB)
	
	def name(fname):
		return os.path.splitext(os.path.basename(fname))[0]
    
	dbs = [RankingDatabase(fname=fname, name=name(fname)) for fname in db_fnames]
	dbs

	#Convert annData object into pandas dataframe
	ex_matrix_filtered = pd.DataFrame(adata.X)
	#Add colnames = genes, rownames = cells
	ex_matrix_filtered.columns = adata.var.index.values
	ex_matrix_filtered.index = adata.obs.index.values

	#write filtered matrix
	ex_matrix_filtered.to_csv(os.path.join(OUTFOLDER, "filtered_matrix.csv"), index=True)

