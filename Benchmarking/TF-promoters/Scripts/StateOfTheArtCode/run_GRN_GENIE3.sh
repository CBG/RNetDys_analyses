#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

## BJ
Rscript run_GRN_GENIE3.R "GSE113415_BJ_FPKM.txt" 2
Rscript run_GRN_GENIE3.R "GSE160910_single_count_BJ.txt" 2
Rscript run_GRN_GENIE3.R "GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt" 2
Rscript run_GRN_GENIE3.R "GSE100344_BJ_FPKM.txt" 2

## GM12878
Rscript run_GRN_GENIE3.R "GSE81861_GM12878_count_batch2.txt" 2
Rscript run_GRN_GENIE3.R "GSM4156602_GM12878_count_rep2.txt" 2
Rscript run_GRN_GENIE3.R "GGSM3596321_GM12878.txt" 2
Rscript run_GRN_GENIE3.R "GSM4156603_GM12878_count_rep3.txt" 2

## H1-ESC
Rscript run_GRN_GENIE3.R "GSE64016_H1ESC_normalized.txt" 2
Rscript run_GRN_GENIE3.R "GSE75748_H1ESC.txt" 2
Rscript run_GRN_GENIE3.R "GSE81861_H1ESC_count_batch1.txt" 2
Rscript run_GRN_GENIE3.R "GSM5534158_H1.txt" 2

## A549
Rscript run_GRN_GENIE3.R "GSE81861_A549_scRNAseq.txt" 2
Rscript run_GRN_GENIE3.R "GSM3271042_scRNAseq_A549.txt" 2

## Jurkat
Rscript run_GRN_GENIE3.R "Jurkat_GSE105451_scRNAseq.txt" 2
Rscript run_GRN_GENIE3.R "Jurkat_scRNAseq.txt" 2

## K-562
Rscript run_GRN_GENIE3.R "GSE113415_K562_FPKM.txt" 2
Rscript run_GRN_GENIE3.R "GSE81861_K562_count.txt" 2
Rscript run_GRN_GENIE3.R "GSE90063_k562_umi_wt.txt" 2
Rscript run_GRN_GENIE3.R "GSM1599500_K562_cells.txt" 2

conda deactivate