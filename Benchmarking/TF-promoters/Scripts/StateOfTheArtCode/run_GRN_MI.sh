#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 5
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

module load lang/Julia/1.6.2-linux-x86_64

## BJ
Rscript run_GRN_MI.R "GSE113415_BJ_FPKM.txt" "d1"
Rscript run_GRN_MI.R "GSE160910_single_count_BJ.txt" "d2"
Rscript run_GRN_MI.R "GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt" "d3"
Rscript run_GRN_MI.R "GSE100344_BJ_FPKM.txt" "d4"

## GM12878
Rscript run_GRN_MI.R "GSE81861_GM12878_count_batch2.txt" "d5"
Rscript run_GRN_MI.R "GSM4156602_GM12878_count_rep2.txt" "d6"
Rscript run_GRN_MI.R "GGSM3596321_GM12878.txt" "d7"
Rscript run_GRN_MI.R "GSM4156603_GM12878_count_rep3.txt" "d8"

## H1-ESC
Rscript run_GRN_MI.R "GSE64016_H1ESC_normalized.txt" "d9"
Rscript run_GRN_MI.R "GSE75748_H1ESC.txt" "d10"
Rscript run_GRN_MI.R "GSE81861_H1ESC_count_batch1.txt" "d11"
Rscript run_GRN_MI.R "GSM5534158_H1.txt" "d12"

## A549
Rscript run_GRN_MI.R "GSE81861_A549_scRNAseq.txt" "d13"
Rscript run_GRN_MI.R "GSM3271042_scRNAseq_A549.txt" "d14"

## Jurkat
Rscript run_GRN_MI.R "Jurkat_GSE105451_scRNAseq.txt" "d15"
Rscript run_GRN_MI.R "Jurkat_scRNAseq.txt" "d16"

## K-562
Rscript run_GRN_MI.R "GSE113415_K562_FPKM.txt" "d17"
Rscript run_GRN_MI.R "GSE81861_K562_count.txt" "d18"
Rscript run_GRN_MI.R "GSE90063_k562_umi_wt.txt" "d19"
Rscript run_GRN_MI.R "GSM1599500_K562_cells.txt" "d20"

conda deactivate