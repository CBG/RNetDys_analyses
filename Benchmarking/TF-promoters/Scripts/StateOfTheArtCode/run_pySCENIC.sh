#!/bin/bash -l
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch


conda activate r4-base-clone1

## BJ
Rscript prepSCENICdata.R "GSE113415_BJ_FPKM.txt"
Rscript prepSCENICdata.R "GSE160910_single_count_BJ.txt"
Rscript prepSCENICdata.R "GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt"
Rscript prepSCENICdata.R "GSE100344_BJ_FPKM.txt"

## GM12878
Rscript prepSCENICdata.R "GSE81861_GM12878_count_batch2.txt"
Rscript prepSCENICdata.R "GSM4156602_GM12878_count_rep2.txt"
Rscript prepSCENICdata.R "GSM3596321_GM12878.txt"
Rscript prepSCENICdata.R "GSM4156603_GM12878_count_rep3.txt"

## H1-ESC
Rscript prepSCENICdata.R "GSE64016_H1ESC_normalized.txt"
Rscript prepSCENICdata.R "GSE75748_H1ESC.txt"
Rscript prepSCENICdata.R "GSE81861_H1ESC_count_batch1.txt"
Rscript prepSCENICdata.R "GSM5534158_H1.txt"

## A549
Rscript prepSCENICdata.R "GSE81861_A549_scRNAseq.txt"
Rscript prepSCENICdata.R "GSM3271042_scRNAseq_A549.txt"

## Jurkat
Rscript prepSCENICdata.R "Jurkat_GSE105451_scRNAseq.txt"
Rscript prepSCENICdata.R "Jurkat_scRNAseq.txt"

## K-562
Rscript prepSCENICdata.R "GSE113415_K562_FPKM.txt"
Rscript prepSCENICdata.R "GSE81861_K562_count.txt"
Rscript prepSCENICdata.R "GSE90063_k562_umi_wt.txt"
Rscript prepSCENICdata.R "GSM1599500_K562_cells.txt"

conda deactivate


export OPENBLAS_NUM_THREADS=1

OUTP="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/SCENIC/TFGene/"
SCENICPATH="/scratch/users/cbarlier/GRN2021/Benchmarking/VERSION2022/PREDICTIONS/SCENIC/"

#Create folders outputs
cd $OUTP
mkdir DBJ1
mkdir DBJ2
mkdir DBJ3
mkdir DBJ4
mkdir DGM1
mkdir DGM2
mkdir DGM3
mkdir DGM4
mkdir DH1
mkdir DH2
mkdir DH3
mkdir DH4
mkdir DA1
mkdir DA2
mkdir DJ1
mkdir DJ2
mkdir DK1
mkdir DK2
mkdir DK3
mkdir DK4

#Go back to root SCENIC
cd $SCENICPATH

conda activate pyscenic_env

## BJ
BJ1="TFGene_50exp_GSE113415_BJ_FPKM.txt.csv"
python pySCENIC.py $OUTP $BJ1 False $OUTP"DBJ1/"
pyscenic grn -o $OUTP"DBJ1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DBJ1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

BJ2="TFGene_50exp_GSE160910_single_count_BJ.txt.csv"
python pySCENIC.py $OUTP $BJ2 True $OUTP"DBJ2/"
pyscenic grn -o $OUTP"DBJ2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DBJ2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

BJ3="TFGene_50exp_GSE166935_BJ_GSM5088689_FibmodifiedCCM.txt.csv"
python pySCENIC.py $OUTP $BJ3 True $OUTP"DBJ3/"
pyscenic grn -o $OUTP"DBJ3/modules.csv" --seed 1234 --num_workers 3 $OUTP"DBJ3/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

BJ4="TFGene_50exp_GSE100344_BJ_FPKM.txt.csv"
python pySCENIC.py $OUTP $BJ4 False $OUTP"DBJ4/"
pyscenic grn -o $OUTP"DBJ4/modules.csv" --seed 1234 --num_workers 3 $OUTP"DBJ4/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

## GM12878
GM1="TFGene_50exp_GSE81861_GM12878_count_batch2.txt.csv"
python pySCENIC.py $OUTP $GM1 True $OUTP"DGM1/"
pyscenic grn -o $OUTP"DGM1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DGM1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

GM2="TFGene_50exp_GSM4156602_GM12878_count_rep2.txt.csv"
python pySCENIC.py $OUTP $GM2 True $OUTP"DGM2/"
pyscenic grn -o $OUTP"DGM2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DGM2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

GM3="TFGene_50exp_GGSM3596321_GM12878.txt.csv"
python pySCENIC.py $OUTP $GM3 True $OUTP"DGM3/"
pyscenic grn -o $OUTP"DGM3/modules.csv" --seed 1234 --num_workers 3 $OUTP"DGM3/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

GM4="TFGene_50exp_GSM4156603_GM12878_count_rep3.txt.csv"
python pySCENIC.py $OUTP $GM4 True $OUTP"DGM4/"
pyscenic grn -o $OUTP"DGM4/modules.csv" --seed 1234 --num_workers 3 $OUTP"DGM4/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

## H1-ESC
H1="TFGene_50exp_GSE64016_H1ESC_normalized.txt.csv"
python pySCENIC.py $OUTP $H1 False $OUTP"DH1/"
pyscenic grn -o $OUTP"DH1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DH1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

H2="TFGene_50exp_GSE75748_H1ESC.txt.csv"
python pySCENIC.py $OUTP $H2 True $OUTP"DH2/"
pyscenic grn -o $OUTP"DH2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DH2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

H3="TFGene_50exp_GSE81861_H1ESC_count_batch1.txt.csv"
python pySCENIC.py $OUTP $H3 True $OUTP"DH3/"
pyscenic grn -o $OUTP"DH3/modules.csv" --seed 1234 --num_workers 3 $OUTP"DH3/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

H4="TFGene_50exp_GSM5534158_H1.txt.csv"
python pySCENIC.py $OUTP $H4 True $OUTP"DH4/"
pyscenic grn -o $OUTP"DH4/modules.csv" --seed 1234 --num_workers 3 $OUTP"DH4/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

## A549
A1="TFGene_50exp_GSE81861_A549_scRNAseq.txt.csv"
python pySCENIC.py $OUTP $A1 True $OUTP"DA1/"
pyscenic grn -o $OUTP"DA1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DA1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

A2="TFGene_50exp_GSM3271042_scRNAseq_A549.txt.csv"
python pySCENIC.py $OUTP $A2 True $OUTP"DA2/"
pyscenic grn -o $OUTP"DA2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DA2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

## Jurkat
J1="TFGene_50exp_Jurkat_GSE105451_scRNAseq.txt.csv"
python pySCENIC.py $OUTP $J1 True $OUTP"DJ1/"
pyscenic grn -o $OUTP"DJ1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DJ1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

J2="TFGene_50exp_Jurkat_scRNAseq.txt.csv"
python pySCENIC.py $OUTP $J2 True $OUTP"DJ2/"
pyscenic grn -o $OUTP"DJ2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DJ2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

## K-562
K1="TFGene_50exp_GSE113415_K562_FPKM.txt.csv"
python pySCENIC.py $OUTP $K1 False $OUTP"DK1/"
pyscenic grn -o $OUTP"DK1/modules.csv" --seed 1234 --num_workers 3 $OUTP"DK1/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

K2="TFGene_50exp_GSE81861_K562_count.txt.csv"
python pySCENIC.py $OUTP $K2 True $OUTP"DK2/"
pyscenic grn -o $OUTP"DK2/modules.csv" --seed 1234 --num_workers 3 $OUTP"DK2/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

K3="TFGene_50exp_GSE90063_k562_umi_wt.txt.csv"
python pySCENIC.py $OUTP $K3 True $OUTP"DK3/"
pyscenic grn -o $OUTP"DK3/modules.csv" --seed 1234 --num_workers 3 $OUTP"DK3/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

K4="TFGene_50exp_GSM1599500_K562_cells.txt.csv"
python pySCENIC.py $OUTP $K4 True $OUTP"DK4/"
pyscenic grn -o $OUTP"DK4/modules.csv" --seed 1234 --num_workers 3 $OUTP"DK4/filtered_matrix.csv" $SCENICPATH"ressources/hs_hgnc_curated_tfs.txt"

conda deactivate




