library(dplyr)
library(data.table)
library(stringr)
library(tidyr)
library(ggplot2)

##OMIM processing
disease<-c("Alzheimer", "Parkinson", "Epilepsy")
OMIM <- read.delim("OMIM_morbidmap-processed.txt") # manually removed rows containing metadata and "?" because the are not vlaidated gene-disease connections
for (i in disease) {
  OMIM_disease <- OMIM %>% filter(grepl(i, OMIM$Phenotype))
  OMIM_p<-OMIM_disease %>% separate_rows(Gene.Symbols, sep = ", ")
  write.table(OMIM_p, paste0(i, "_OMIM.txt"), sep = "\t", col.names = TRUE, row.names = FALSE, quote = F)
}


##eQTL processing
celltypes<-c("Astrocytes.", "Excitatory.neurons.", "Inhibitory.neurons.", "Microglia.", "Oligodendrocytes.", "OPCs...COPs.")
for (i in celltypes) {
  merge <- data.frame()
  for (n in 1:22) {
    mtx <- read.delim(paste0("Zenodo/",i,n,".gz"), sep = " ", header = FALSE) #obtained from https://zenodo.org/record/6104982#.Yt5qlIRByUl
    mtx_p <- mtx %>% separate(V1, c("Gene.Symbols", "Ensembl"), sep = "_")
    colnames(mtx_p)[3] <- "RSID"
    merge<-rbind(merge,mtx_p)
    rm(mtx)
    rm(mtx_p)
  }
  write.table(merge, paste0(i, "all_eQTL.txt"), sep = "\t", quote = F, row.names = FALSE, col.names = TRUE)
}

##RNetDys processing  
TS4 <- read.delim("TableS4.txt", sep= "\t")
TS4_celltypes <- unique(TS4$Cell_pop)
for (i in TS4_celltypes) {
  TS4_sub <- TS4 %>% filter(grepl(i, TS4$Cell_pop))
  write.table(TS4_sub, paste0(i, ".txt"), sep = "\t", quote = F, row.names = FALSE, col.names = TRUE)
  rm(TS4_sub)
}



##eQTL matching OMIM
df <- data.frame(matrix(NA,ncol = 4))
colnames(df) <- c('Disease', 'CellType', 'GeneMatch', 'GeneNMatch')
for (i in celltypes) {
  mtx <- read.delim(paste0(i,"all_eQTL.txt"), sep = "\t", header = TRUE)
  for (n in disease) {
    df_disease <- read.delim(paste0(n,"_OMIM.txt"), sep = "\t", header = TRUE)
    disease_results <- data.frame("Disease"=n,
                                  "CellType"=i,
                                  "GeneMatch" = length(which(unique(mtx$Gene.Symbols) %in% df_disease$Gene.Symbols)),
                                  "GeneNMatch" = length(which(!unique(mtx$Gene.Symbols) %in% df_disease$Gene.Symbols)))
    df <- rbind(df,disease_results)
  }
}
df$ratio <- df$GeneMatch / (df$GeneMatch + df$GeneNMatch)
saveRDS(df, file = "disease_eQTL_OMIM_matched.rds")


##RNetDys matching OMIM
df_RND <- data.frame(matrix(NA,ncol = 4))
colnames(df_RND) <- c('Disease', 'CellType', 'GeneMatch', 'GeneNMatch')
for (i in TS4_celltypes) {
  mtx <- read.delim(paste0(i,".txt"), sep = "\t", header = TRUE)
  for (n in disease) {
    df_disease <- read.delim(paste0(n,"_OMIM.txt"), sep = "\t", header = TRUE)
    disease_results <- data.frame("Disease"=n,
                                  "CellType"=i,
                                  "GeneMatch" = length(which(unique(mtx$Gene.Symbols) %in% df_disease$Gene.Symbols)),
                                  "GeneNMatch" = length(which(!unique(mtx$Gene.Symbols) %in% df_disease$Gene.Symbols)))
    df_RND <- rbind(df_RND,disease_results)
  }
}
df_RND$ratio <- df_RND$GeneMatch / (df_RND$GeneMatch + df_RND$GeneNMatch)
saveRDS(df_RND, file = "disease_OMIM_matched.rds")

df <- readRDS("disease_eQTL_OMIM_matched.rds")
df_RND <- readRDS("disease_OMIM_matched.rds")
df$Method <- "eQTL"
df_RND$Method <- "RNetDys"
df_results <- rbind(df,df_RND)
df_results <- df_results[!is.na(df_results$Disease),]
write.table(df_results, "disease_final.txt", sep = "\t", quote = F, row.names = FALSE, col.names = TRUE)

#remove DA neurons since they are not in eQTL data
df_results <- read.delim("disease_final.txt")
ggplot(df_results, aes(x=Method, y=ratio, fill=Method)) + 
  geom_boxplot() + 
  xlab("Method")
