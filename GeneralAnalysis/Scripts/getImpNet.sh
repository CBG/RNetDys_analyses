#!/bin/sh

IMPPATH="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedReg/ImpReg/"
NETPATH="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/RNetDysNet/"
OUTPATH="/Users/celine.barlier/Desktop/REPOSITORIES/GITLAB/RNetDys_analyses/GeneralAnalysis/ImpairedNet/"

#AD
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_Astro_ImpReg.txt" $NETPATH"MDFG_Astro_S2PEreg_net.txt" $OUTPATH"AD_MDFG_Astro_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_Ex_ImpReg.txt" $NETPATH"MDFG_Ex_S2PEreg_net.txt" $OUTPATH"AD_MDFG_Ex_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_Inh_ImpReg.txt" $NETPATH"MDFG_Inh_S2PEreg_net.txt" $OUTPATH"AD_MDFG_Inh_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_Mic_ImpReg.txt" $NETPATH"MDFG_Mic_S2PEreg_net.txt" $OUTPATH"AD_MDFG_Mic_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_Oligo_ImpReg.txt" $NETPATH"MDFG_Oligo_S2PEreg_net.txt" $OUTPATH"AD_MDFG_Oligo_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"AD_MDFG_OPCs_ImpReg.txt" $NETPATH"MDFG_OPCs_S2PEreg_net.txt" $OUTPATH"AD_MDFG_OPCs_ImpReg.txt"

#PD
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"PD_SUNI_Astro_ImpReg.txt" $NETPATH"SUNI_Astro_S2PEreg_net.txt" $OUTPATH"PD_SUNI_Astro_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"PD_SUNI_DAn_ImpReg.txt" $NETPATH"SUNI_DAn_S2PEreg_net.txt" $OUTPATH"PD_SUNI_DAn_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"PD_SUNI_Ex_ImpReg.txt" $NETPATH"SUNI_Ex_S2PEreg_net.txt" $OUTPATH"PD_SUNI_Ex_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"PD_SUNI_Oligo_ImpReg.txt" $NETPATH"SUNI_Oligo_S2PEreg_net.txt" $OUTPATH"PD_SUNI_Oligo_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"PD_SUNI_OPCs_ImpReg.txt" $NETPATH"SUNI_OPCs_S2PEreg_net.txt" $OUTPATH"PD_SUNI_OPCs_ImpReg.txt"

#EPI
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"EPI_MDFG_Astro_ImpReg.txt" $NETPATH"MDFG_Astro_S2PEreg_net.txt" $OUTPATH"EPI_MDFG_Astro_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"EPI_MDFG_Ex_ImpReg.txt" $NETPATH"MDFG_Ex_S2PEreg_net.txt" $OUTPATH"EPI_MDFG_Ex_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"EPI_MDFG_Mic_ImpReg.txt" $NETPATH"MDFG_Mic_S2PEreg_net.txt" $OUTPATH"EPI_MDFG_Mic_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"EPI_MDFG_Oligo_ImpReg.txt" $NETPATH"MDFG_Oligo_S2PEreg_net.txt" $OUTPATH"EPI_MDFG_Oligo_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"EPI_MDFG_OPCs_ImpReg.txt" $NETPATH"MDFG_OPCs_S2PEreg_net.txt" $OUTPATH"EPI_MDFG_OPCs_ImpReg.txt"

#T1D
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T1D_Alpha_ImpReg.txt" $NETPATH"Alpha_cells_S2PEreg_net.txt" $OUTPATH"T1D_Alpha_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T1D_Beta_ImpReg.txt" $NETPATH"Beta_cells_S2PEreg_net.txt" $OUTPATH"T1D_Beta_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T1D_Delta_ImpReg.txt" $NETPATH"Delta_cells_S2PEreg_net.txt" $OUTPATH"T1D_Delta_ImpReg.txt"

#T2D
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T2D_Alpha_ImpReg.txt" $NETPATH"Alpha_cells_S2PEreg_net.txt" $OUTPATH"T2D_Alpha_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T2D_Beta_ImpReg.txt" $NETPATH"Beta_cells_S2PEreg_net.txt" $OUTPATH"T2D_Beta_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T2D_Delta_ImpReg.txt" $NETPATH"Delta_cells_S2PEreg_net.txt" $OUTPATH"T2D_Delta_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"T2D_Gamma_ImpReg.txt" $NETPATH"Gamma_cells_S2PEreg_net.txt" $OUTPATH"T2D_Gamma_ImpReg.txt"

#SLE
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"SLE_Breg_ImpReg.txt" $NETPATH"PBMC_Breg_S2PEreg_net.txt" $OUTPATH"SLE_Breg_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"SLE_CD8naive_ImpReg.txt" $NETPATH"PBMC_CD8_naive_S2PEreg_net.txt" $OUTPATH"SLE_CD8naive_ImpReg.txt"
/usr/local/bin/Rscript buildImpNet.R $IMPPATH"SLE_CD16mono_ImpReg.txt" $NETPATH"PBMC_CD16_monocytes_S2PEreg_net.txt" $OUTPATH"SLE_CD16mono_ImpReg.txt"

