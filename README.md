# RNetDys project analyses

RNetDys is a systematic pipeline to decipher cell (sub)type specific regulatory interactions impaired by disease-related SNPs. This pipeline relies on multi-OMICS data to provide mechanistic insights into impaired regulatory interactions due to SNPs by leveraging the information obtained from the GRN inference. The pipeline contains two main parts composed of (1) the cell (sub)type specific GRN inference and (2) the capture of impaired regulatory interactions due to disease-related SNPs to gain regulatory mechanistic insights for the disease.

**RNetDys pipeline: https://github.com/BarlierC/RNetDys**

We applied RNetDys in five disease case including Alzheimer’s disease (AD), Parkinson’s disease (PD), Epilepsy (EPI), Diabetes type I (T1D) and type II (T2D), to study the cell (sub)type differential impairment due to SNPs and validated the relevance of the predicted impaired regulatory interactions using Genome-Wide Association Studies (GWAS) and expression Quantitative Trait Loci (eQTLs) data. This repository contains the scripts for reproducibility of the analyses performed.

**"RNetDys: identification of disease-related impaired regulatory interactions due to SNPs" C. Barlier, M. Ribeiro et al. (Manuscript in preparation)**

# Content of the repository

- **Benchmarking**: scripts used to performed the TF-genes and Enhancer-promoter benchmarking of RNetDys compared to state-of-the-art methods
- **DataProcessing**: scripts to process and extract the scRNA-seq and scATAC-seq datasets of brain and pancreas cell (sub)types 
- **GeneralAnalysis**: scripts used to perform the study of impaired regulatory interactions for the five diseases
- **RNetDysNet**: GRNs inferred by RNetDys
- **ThresholdDefinition**: script to identify the best cutoff to use for the intersection of ChIP-seq and scATAC-seq data
